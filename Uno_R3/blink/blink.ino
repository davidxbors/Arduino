/*
   textToMorse example
   by __z01db3rg
   5.6.18
*/

int LED1 = 12; //pin of LED1
char* strng = "SO";

int dotPause = 750;
int linePause = 1500;
int betweenPause = 250;
int boundaryPause = 450;

namespace hardwareMode {
void putMorse(char* str) {
  for(int i = 0; i < strlen(str); ++i){
    if(str[i] == '.'){
        digitalWrite(LED1, HIGH);
        delay(dotPause);
        digitalWrite(LED1, LOW);
        delay(betweenPause);
      } else {
        digitalWrite(LED1, HIGH);
        delay(linePause);
        digitalWrite(LED1, LOW);
        delay(betweenPause);        
        } 
  }
  delay(boundaryPause);
}

};

struct dict {
  char ch;
  char* val;
};

dict d[] = {
  {'a', ".-"},
  {'b', "-..."},
  {'c', "-.-."},
  {'d', "-.."},
  {'e', "."},
  {'f', "..-."},
  {'g', "--."},
  {'h', "...."},
  {'i', ".."},
  {'j', ".---"},
  {'k', "-.-"},
  {'l', ".-.."},
  {'m', "--"},
  {'n', "-."},
  {'o', "---"},
  {'p', ".--."},
  {'q', "--.-"},
  {'r', ".-."},
  {'s', "..."},
  {'t', "-"},
  {'u', "..-"},
  {'v', "...-"},
  {'w', ".--"},
  {'x', "-..-"},
  {'y', "-.--"},
  {'z', "--.."},
  {'1', ".----"},
  {'2', "..---"},
  {'3', "...--"},
  {'4', "....-"},
  {'5', "....."},
  {'6', "-...."},
  {'7', "--..."},
  {'8', "---.."},
  {'9', "----."},
  {'0', "-----"},
  {' ', "/"},
  {'.', ".-.-.-"},
  {'?', "..--.."},
  {',', "--..--"},
};

void toMorse(char* s) {
  using namespace hardwareMode;
  for (int i = 0; i < strlen(s); ++i) {
    if (s[i] > 65 && s[i] < 90)
      s[i] += 32;
    for (int j = 0; j < 46; ++j) {
      if (s[i] == d[j].ch) {
        putMorse(d[j].val);
      }
    }
  }
}



void setup() {
  // put your setup code here, to run once:
  pinMode(LED1, OUTPUT);
}

void loop() {
  /*digitalWrite(LED1, HIGH);
  delay(1000);
  digitalWrite(LED1, LOW); */
  toMorse(strng);
  while(1);
}
