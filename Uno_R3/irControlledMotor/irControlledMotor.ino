#include <Servo.h>
#include <IRremote.h>
#define rpin 11
#define servoPin 7

IRrecv irrecv(rpin);

decode_results results;

Servo servo;
int servoAngle = 0;
int servoSpeed = 100;

void rotate(int speed){
  servo.attach(servoPin);
  for(int a = 0; a <= 180; a++){
    servo.write(a);
    delay(speed);  
  }

  for(int a = 180; a >= 0; --a){
    servo.write(a);
    delay(speed);
  }
  servo.detach();
}

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  irrecv.enableIRIn();
}

void loop() {
  // put your main code here, to run repeatedly:
  if (irrecv.decode(&results)) {
    switch(results.value)

    {  
    //play/pause button
    /*
     * modifies the on/off state of the motor
     */
    case 0xFFC23D:  
      Serial.println(" PLAY/PAUSE     "); 
      rotate(servoSpeed);
      break;

    //vol- button
    /*
     * reduces the speed of the motor
     */
    case 0xFFE01F:  
      Serial.println(" VOL-           "); 
      if ( servoSpeed + 10 > 100)
        servoSpeed = 100;
      else servoSpeed += 10;
      break;

    //vol+ button
    /*
     * increases the speed of the motor
     */
    case 0xFFA857:  
      Serial.println(" VOL+           "); 
      if (servoSpeed - 10 < 0)
        servoSpeed = 0;
      else servoSpeed -= 10;
      break;
   
    default: 
      Serial.println(" other button   ");
    }
    
    irrecv.resume();
  }
  delay(300);
}
