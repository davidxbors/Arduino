#define redLed 12 //0
#define yellowLed 8  //1
#define greenLed 7 //2
#define button 4

int current = 1;
//int flag = 0;

void changeLights(){
  if (current == 2) current = 0;
  else ++current;  
  switch (current){
    case 0:
      digitalWrite(redLed, HIGH);
      delay(1000);
      if (digitalRead(button) == HIGH){
        digitalWrite(redLed, LOW);
        break;
      }
      delay(1000);
      if (digitalRead(button) == HIGH){
        digitalWrite(redLed, LOW);
        break;
      }
      delay(1000);
      digitalWrite(redLed, LOW);
      break;
    case 1:
      digitalWrite(yellowLed, HIGH);
      delay(1000);
      digitalWrite(yellowLed, LOW);
      break;
    case 2:
      digitalWrite(greenLed, HIGH);
      delay(3000);
      digitalWrite(greenLed, LOW);
      break;
  }
}

void setup() {
  pinMode(redLed, OUTPUT);
  pinMode(yellowLed, OUTPUT);
  pinMode(greenLed, OUTPUT);  
  pinMode(button, INPUT);  
}

void loop() {
  changeLights();
//  if(digitalRead(button) == HIGH){
//    if(!flag){
//      digitalWrite(redLed, HIGH);
//      flag = 1;
//    }
//    else if(flag){
//      digitalWrite(redLed, LOW);
//      flag = 0; 
//    }
//  }
//  delay(200);
}
