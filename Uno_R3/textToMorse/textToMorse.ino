/*
 * Date: 10.06.2018
 * Author: David Bors
 * Title: textToMorse
 * Description: takes a string and converts it to morse code, than
 * outputs it using a led
 * Variables for integration with what you want: 
 * led integer is the led pin
 * s char pointer is the string to be translated           
 * line/dot Time the time the led will stay on for a line / dot
 * delayPause the pause between each sign
 */

int led = 12;
char* s = "felicitari, Ioana. Mult succes mai departe.";
int lineTime = 1500, dotTime = 750, delayPause = 500;

struct dict {
  char ch;
  char* val;
};

dict d[] = {
  {'a', ".-"},
  {'b', "-..."},
  {'c', "-.-."},
  {'d', "-.."},
  {'e', "."},
  {'f', "..-."},
  {'g', "--."},
  {'h', "...."},
  {'i', ".."},
  {'j', ".---"},
  {'k', "-.-"},
  {'l', ".-.."},
  {'m', "--"},
  {'n', "-."},
  {'o', "---"},
  {'p', ".--."},
  {'q', "--.-"},
  {'r', ".-."},
  {'s', "..."},
  {'t', "-"},
  {'u', "..-"},
  {'v', "...-"},
  {'w', ".--"},
  {'x', "-..-"},
  {'y', "-.--"},
  {'z', "--.."},
  {'1', ".----"},
  {'2', "..---"},
  {'3', "...--"},
  {'4', "....-"},
  {'5', "....."},
  {'6', "-...."},
  {'7', "--..."},
  {'8', "---.."},
  {'9', "----."},
  {'0', "-----"},
  {' ', "/"},
  {'.', ".-.-.-"},
  {'?', "..--.."},
  {',', "--..--"},
};

void linePut(){
  digitalWrite(led, HIGH);
  delay(lineTime);
  digitalWrite(led, LOW);
  delay(delayPause);
}

void dotPut(){
  digitalWrite(led, HIGH);
  delay(dotTime);
  digitalWrite(led, LOW);
  delay(delayPause);
}

void spacePut(){
  delay(dotTime);  
}

void putMorse(char* s){
  for(int i = 0; i < strlen(s); ++i){
    if(s[i] == '.')
      dotPut();
    else if(s[i] == '/')
      spacePut();
    else linePut();  
  }
}

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(led, OUTPUT);
    for(int i = 0; i < strlen(s); ++i){
    if (s[i] > 65 && s[i] < 90)
      s[i] += 32;
    for (int j = 0; j < 40; ++j){
        if (s[i] == d[j].ch){
          putMorse(d[j].val);
        }
      }
    }
}

void loop() {
  // put your main code here, to run repeatedly:

}
