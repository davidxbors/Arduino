#include <Servo.h>
#define servoPin 7

Servo servo;

int servoAngle = 0;

void rotate(int speed){
  for(int a = 0; a <= 180; a++){
    servo.write(a);
    delay(speed);  
  }

  for(int a = 180; a >= 0; --a){
    servo.write(a);
    delay(speed);
  }
}

void setup() {
  Serial.begin(9600);
  servo.attach(servoPin);

}

void loop() {
//  servo.write(45);
//  delay(1000);
//  servo.write(90);
//  delay(1000);
//  servo.write(135);
//  delay(1000);
//  servo.write(90);
//  delay(1000);
  rotate(100);
  servo.detach();
  while(true){} //end this loop
}
