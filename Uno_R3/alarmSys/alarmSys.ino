#include <NewPing.h>

#define trig 7
#define echo 8
#define MAX_DIST 200
#define led 12
#define target 130

NewPing sonar(trig, echo, MAX_DIST);

void alarm(){
  Serial.println("Alarm!"); 
    digitalWrite(led, HIGH);
    delay(200);
    digitalWrite(led, LOW);
    delay(200);
}

void setup() {
  pinMode(led, OUTPUT); 
  Serial.begin(115200);
}

void loop() {
  delay(50);
  unsigned int uS = sonar.ping();
  int distance = uS / US_ROUNDTRIP_CM;

  if (distance < target)
    alarm();
  
  // dev only
  Serial.print("Ping: ");
  Serial.print(distance);
  Serial.println("cm");
}

